/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h> 

/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
  LRU,
NRU,
 RRIP,
 RANDOM 
};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};
enum optimization {
  L2,
  VC,
  None
};
/*
 * STRUCTS
 */

/* Cache tag array fields */
struct entry {
 bool valid ;
 bool dirty;
 int tag ;
 uint8_t rp_value ;
};

/* Cache replacement policy results */
struct operation_result {
 enum miss_hit_status miss_hit;
 bool dirty_eviction;
 int  evicted_address;
};

/* 
 *  Functions
 * /
/*************************************************************/
/*
 * Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   int *tag_size,
                   int *idx_size,
                   int *offset_size);
/*************************************************************/
/* 
 * Get tag and index from address
 * 
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         int tag_size,
                         int idx_size,
                         int offset_size,
                         int *idx,
                         int *tag);

/*************************************************************/
/* 
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(hp) policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int srrip_replacement_policy (int idx,
                              int tag,
                              int associativity,
                              bool loadstore,
                              entry* cache_blocks,
                              operation_result* operation_result,
                              bool debug=false);
/*************************************************************/
/* 
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy (int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);

/*************************************************************/
/*
 * Creates a 2D Array to represent the Cache
 * Ways -> Columns
 * Sets -> Rows
 * 
 * LRU counter are initialized the following way
 * Way0 = 0 ; Way1 = 1 ; Way2 = 2; ... ; WayN = N
 * 
 * RRPV values are initialized all in Assoc-1.
 * 
 * [in] associativity: number of ways of the entry
 * [in] idx_size: number of bits of the index field
 * [in] rp: replacemen pol.
 * 
 * [return] cache
 */
entry **create_cache (int associativity,
                     int idx_size,
                     bool rp);

/*************************************************************/
/*Helper Functions*/
/*
 * Checks if int is power of 2
 * [in] v: value to check
 * [return] isPow: boolean
 */
bool pow_of_2(int v);

/*************************************************************/
/*Recieves all final results and counts to present to the user.*/
void  print_Sim(  int cachesize_kb,
                  int associativity,
                  int blocksize_bytes,
                  double miss_rate,
                  int dirty_evictions,
                  int misses,
                  int hits);

/*************************************************************/


void get_Args(int argc,
              char *argv[],
              int *cachesize_kb,
              int *blocksize_bytes,
              int *associativity,
              bool *rp,
              int *opt);


/**********************CACHE L2**********************/

 /* Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] L2associativity
 * [out] L2size_kb
 * [out] L2tag_size: size in bits of the tag field
 * [out] L2idx_size: size in bits of the index field
 * [out] L2offset_size: size in bits of the offset size
 */
int L2field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *L2associativity,
                    int *L2size_kb,
                    int *L2tag_size,
                    int *L2idx_size,
                    int *L2offset_size); 


/*
 * Creates a 2D Array to represent the L2 Cache
 * Ways -> Columns
 * Sets -> Rows
 * 
 * LRU counter are initialized the following way
 * Way0 = 0 ; Way1 = 1 ; Way2 = 2; ... ; WayN = N
 * 
 * 
 * [in] L2associativity: number of ways of the entry
 * [in] L2idx_size: number of bits of the index field
 * 
 * [return] L2cache
 */
entry **create_L2cache (int L2associativity,
                        int L2idx_size
                        );


/*************************************************************/
/* Recieves evicted data from L1
 * Reconstructs evicted address
 * Calculates Tag and Index with the address 
 * 
 * [in] evicted_Tag: Tag from evicted address, from L1 
 * [in] evicted_idx: Index from evicted addres, from L1
 * [in] idx_size: L1 index size
 * [in] L2tag_size: number of bits of the tag field
 * [in] L2idx_size: number of bits of the index field
 * [in] L2offset_size: number of bits of the offset field
 *
 * [out] L2idx: cache line idx
 * [out] L2tag: cache line tag
 */

void L2address_tag_idx_get(int evicted_Tag,
                           int evicted_idx,
                           int idx_size,
                           int L2tag_size,
                           int L2idx_size,
                           int L2offset_size,
                           int *L2idx,
                           int *L2tag);  

/*************************************************************/
/*Recieves all final results and counts to present to the user.*/
void L2print_Sim(int cachesize_kb,
                 int L2cachesize_kb,
                 int associativity,
                 int L2associativity,
                 int blocksize_bytes,
                 double miss_rate,
                 double L2miss_rate,
                 double Overall_miss_rate, 
                 int misses,
                 int hits,
                 int L2misses,
                 int L2hits,
                 int L2dirty_evictions);



/*
 * Creates Victim Cache 
 * No inputs. Size is fixed.
 */
entry **create_VC ();

/*
 * Gets VCTag by joining evicted tag and index
 * address = [0..........|TAG|]
 * address = [0....|TAG|000..0]
 * address = [0....|TAG| IDX |]
 * address = [0....|  TAGVC  |]
 */
void VCaddress_tag_get(int evicted_Tag,
                       int evicted_idx,
                       int idx_size,
                       int *VCtag); 

/*
 * Recieves all final results and counts to present to the user.
 */
void VCprint_Sim(int cachesize_kb,
                 int associativity,
                 int blocksize_bytes,
                 double miss_rate,
                 int misses,
                 int hits,
                 int VChits,
                 int VCdirty_evictions);
                       
#endif

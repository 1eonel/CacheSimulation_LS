#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <inttypes.h>
#include <debug_utilities.h>
#include <ctime>

using namespace std;

#define HIT_TIME 1
#define MISS_PENALTY 20


int main(int argc, char * argv []) {

  printf("Do something :), don't forget to keep track of execution time");
  /*Initializations*********************************************/
    int *cachesize_kb = new int;
    int *L2size_kb = new int;
    int *associativity = new int;
    int *L2associativity = new int;
    int *blocksize_bytes = new int;
    int status;
    int IC;
    int IC_count = 0;
    
    int *tag_size = new int;
    int *idx_size = new int;
    int *offset_size = new int; 

    int *L2tag_size = new int;
    int *L2idx_size = new int;
    int *L2offset_size = new int;

    int *idx = new int;
    int *tag = new int;

    int *L2idx = new int;
    int *L2tag = new int;

	int *VCtag = new int;

    int *opt = new int;

	int *zero = new int; *zero=0;
	int *sixteen = new int; *sixteen=16;

    int dirty_evictions = 0;
    int misses_Load = 0;
    int misses_Store = 0;
    int hits_Load = 0;
    int hits_Store = 0;
    int hits = 0;
    int misses = 0;

    int L2dirty_evictions = 0;
    int L2misses_Load = 0;
    int L2misses_Store = 0;
    int L2hits_Load = 0;
    int L2hits_Store = 0;
    int L2hits = 0;
    int L2misses = 0;

    int VCdirty_evictions = 0;
    int VCmisses_Load = 0;
    int VCmisses_Store = 0;
    int VChits_Load = 0;
    int VChits_Store = 0;
    int VChits = 0;
    int VCmisses = 0;

    int accesses;
    int L2accesses;
    int AMAT;
    int CPU_time;

    double miss_rate;
    double hit_rate;
    double read_miss_rate;

    bool *rp = new bool;
    bool loadstore;
    bool debug = false;

    long address;

    char num;
    char tracedata[8];
    struct operation_result op_result = {};
    struct operation_result L2op_result = {};
    struct operation_result VCop_result = {};

  /****************************************************************************************************/
  /* Parse argruments */
  get_Args(argc, argv, cachesize_kb, blocksize_bytes, associativity, rp, opt); 
  /*****************************************************************/
  cout << "opt     " << *opt << endl;
  /*Cache creation*/
  status = field_size_get(*cachesize_kb,*associativity,*blocksize_bytes,tag_size,idx_size,offset_size );
  if(status == ERROR){ cout << "field_size_get = ERROR" << endl; exit(0);}

  if (*opt == 0){ //L2 Cache optimization
    status = L2field_size_get(*cachesize_kb, *associativity,*blocksize_bytes,L2associativity,L2size_kb,L2tag_size,L2idx_size,L2offset_size);
    if(status == ERROR){ cout << "L2field_size_get = ERROR" << endl; exit(0);}
  }

  
  /*************CACHE CREATIONS****************/
  /*L1 Cache*/
  entry **cache = create_cache(*associativity, *idx_size, *rp);
  /*L2 or VC*/
  entry **L2cache = create_L2cache(*L2associativity, *L2idx_size);

  entry **VCache = create_VC();

  /* Get trace's lines and start your simulation */
  string hash = "#";

  while (hash == "#")
  {
	  /*****************Read Trace*********************/
	  /*Check for # */			   //
	  hash = "not hash lol";	   //
	  cin >> hash;				   //
								   //
	  /*Read LoadStore*/		   //
	  cin >> tracedata;			   //
	  loadstore = atoi(tracedata); //
	  /*Read Address*/			   //
	  cin >> hex >> address;	   //
								   //
	  /*Read IC*/				   //
	  cin >> tracedata;			   //
	  IC = atoi(tracedata);		   //
	  IC_count = IC_count + IC;	//
	  /************************************************/

	  /***************Process Trace Info***************/
	  /*Get Tag & Index*/
	  address_tag_idx_get(address,
						  *tag_size,
						  *idx_size,
						  *offset_size,
						  idx,
						  tag);
	  /*************************************************/
	  /*******Apply Replacement Algorithm***************/
	  status = lru_replacement_policy(*idx,
									  *tag,
									  *associativity,
									  loadstore,
									  cache[*idx],
									  &op_result,
									  debug);
	  if (status == ERROR)
	  {
		  cout << "L1 lru_replacement_policy = ERROR" << endl;
		  exit(0);
	  }
	  //
	  if (*opt == 0)
	  {
		  /*If Miss on L1, execute access on L2*/
		  if (op_result.miss_hit == MISS_LOAD || op_result.miss_hit == MISS_STORE)
		  {
			  /*Get Tag and Index for L2*/
			  L2address_tag_idx_get(op_result.evicted_address, *idx, *idx_size, *L2tag_size, *L2idx_size, *L2offset_size,
									L2idx, L2tag);

			  status = lru_replacement_policy(*L2idx,
											  *L2tag,
											  *L2associativity,
											  loadstore,
											  L2cache[*L2idx],
											  &L2op_result,
											  debug);
			  if (status == ERROR)
			  {
				  cout << "L2 lru_replacement_policy = ERROR" << endl;
				  exit(0);
			  }
		  }
	  }
	  else if (*opt == 1)
	  {
		  if (op_result.miss_hit == MISS_LOAD || op_result.miss_hit == MISS_STORE)
		  {
			  /*Get Tag for VC*/
			  VCaddress_tag_get(op_result.evicted_address, *idx, *idx_size, VCtag);
			  status = lru_replacement_policy(*zero,
											  *VCtag,
											  *sixteen,
											  loadstore,
											  VCache[0],
											  &VCop_result,
											  debug);
			  if (status == ERROR)
			  {
				  cout << "VC lru_replacement_policy = ERROR" << endl;
				  exit(0);
			  }
		  } /////////////////////////////////////////////
	  }

	  /*********************Post Result Operations**********************/
	  /*Dirty Evictions*/
	  if (op_result.dirty_eviction == true)
		  dirty_evictions++;

	  /*Updates Hit-Miss counters*/
	  /*Hacer Función!!!!*/
	  switch (op_result.miss_hit)
	  {
	  case MISS_LOAD:
		  misses_Load++;
		  break;
	  case MISS_STORE:
		  misses_Store++;
		  break;
	  case HIT_LOAD:
		  hits_Load++;
		  break;
	  case HIT_STORE:
		  hits_Store++;
		  break;
	  }
		/*Optimization Post Result Ops*/
	  if (*opt == 0)
	  {
		  if (op_result.miss_hit == MISS_LOAD || op_result.miss_hit == MISS_STORE)
		  {

			  if (L2op_result.dirty_eviction == true)
				  L2dirty_evictions++;

			  switch (L2op_result.miss_hit)
			  {
			  case MISS_LOAD:
				  L2misses_Load++;
				  break;
			  case MISS_STORE:
				  L2misses_Store++;
				  break;
			  case HIT_LOAD:
				  L2hits_Load++;
				  break;
			  case HIT_STORE:
				  L2hits_Store++;
				  break;
			  }
		  }
	  }else if (*opt == 1)
	  {
		  if (op_result.miss_hit == MISS_LOAD || op_result.miss_hit == MISS_STORE)
		  {
			  if (VCop_result.dirty_eviction == true)
				  VCdirty_evictions++;

			  switch (VCop_result.miss_hit)
			  {
			  case MISS_LOAD:
				  VCmisses_Load++;
				  break;
			  case MISS_STORE:
				  VCmisses_Store++;
				  break;
			  case HIT_LOAD:
				  VChits_Load++;
				  break;
			  case HIT_STORE:
				  VChits_Store++;
				  break;
			  }
			  
		  }
		  
	  }
  }
  /****end while******/

  /*Final Hit-Miss count*/
  hits = hits_Load + hits_Store;
  misses = misses_Load + misses_Store;
  accesses = hits + misses;

  /*Hit/Miss Rate*/
  miss_rate = double(misses) / double(accesses);
  hit_rate = double(hits) / double(accesses);
  read_miss_rate = double(misses_Load) / double(misses);

  if (*opt == 0)
  {

	  /*Final L2 Hit-Miss count*/
	  L2hits = L2hits_Load + L2hits_Store;
	  L2misses = L2misses_Load + L2misses_Store;

	  /*L2 miss rate*/
	  double L2miss_rate = double(L2misses) / double(misses);

	  /*Overall miss rate*/
	  double Overall_miss_rate = double(misses + L2misses) / double(accesses);

	  L2print_Sim(*cachesize_kb,
				  *L2size_kb,
				  *associativity,
				  *L2associativity,
				  *blocksize_bytes,
				  miss_rate,
				  L2miss_rate,
				  Overall_miss_rate,
				  misses,
				  hits,
				  L2misses,
				  L2hits,
				  L2dirty_evictions);
  }
  else if (*opt == 1)
  {
	  /*Final VC Hit-Miss count*/
	  VChits = VChits_Load + VChits_Store;
	  VCmisses = VCmisses_Load + VCmisses_Store;
	
	  /*L1 and VC Miss Rate*/
	  miss_rate = double (VCmisses + misses) / double(accesses);
	  /*L1 and VC Misses*/
	  misses = misses + VCmisses;
	  hits = hits + VChits;

	  VCprint_Sim(*cachesize_kb,
	  			  *associativity,
				  *blocksize_bytes,
				  miss_rate,
				  misses,
				  hits,
				  VChits,
				  VCdirty_evictions);
  }else
  {
	  print_Sim(*cachesize_kb,
	  			*associativity,
				*blocksize_bytes,
				miss_rate,
				dirty_evictions,
				misses,
				hits);
  }
  

  /*********************************************************************/
  /* Print cache configuration */

  /* print_Sim(  *cachesize_kb,
              *associativity,
              *blocksize_bytes,
              CPU_time,
              AMAT,
              miss_rate,
              read_miss_rate,
              dirty_evictions,
              misses_Load,
              misses_Store,
              misses,
              hits_Load,
              hits_Store,
              hits,
              accesses);*/

  return 0;
}

























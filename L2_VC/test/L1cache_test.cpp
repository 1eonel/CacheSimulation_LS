



/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#define YEL "\x1B[33m"
using namespace std;
/* Globals */
bool debug = true;

TEST(VCache, miss_hit)
{
  int associativity;
  int address_A;
  int status;
  int *zero = new int;
  *zero = 0;
  int *sixteen = new int;
  *sixteen = 16;
  //****************************1. Choose a random associativity    **********************************
  associativity = 1 << (rand() % 4); 

  //************************* 2. Choose a random address (AddressA)  ********************************

  int idx_size = rand() % 8 + 1;
  int tag_random = rand() % 4096;
  int idx_random = rand() % ((int)pow(2, idx_size));
  int tag_A = tag_random;
  int idx_A = idx_random;

  address_A = tag_random;
  address_A << idx_size;
  address_A = address_A | idx_random;

  //**************************** 3. Fill a victim cache with random addresses and include AddressA. *****
  bool dirty_random = rand() % 2;
  int random_address;

  entry **VC = create_VC();
  bool randGeneration = true;
  for (int i = 0; i < 16; i++)
  {
    // add tag not in VC already
    randGeneration = true;
    while (randGeneration)
    {
      tag_random = rand() % 4096;
      random_address = tag_random;
      random_address << idx_size;
      random_address = random_address | idx_random;
      while (random_address == address_A) //random different than address_A
      {
        tag_random = rand() % 4096;
        random_address = tag_random;
        random_address << idx_size;
        random_address = random_address | idx_random;
      }
      randGeneration = false;
      for (int j = 0; j < 16; j++)
      {
        if (random_address == VC[0][j].tag & VC[0][j].valid == true)
        {
          randGeneration = true;
        }
      }
    }

    VC[0][i].valid = 1;
    if (i == 8)
    {
      VC[0][i].tag = address_A;
      VC[0][i].dirty = dirty_random;
    }
    else
    {
      VC[0][i].tag = random_address;
      VC[0][i].dirty = 0;
    }
  }

  //***********************4. Fill a cache line with random addresses, making sure AddressA is not added.*******
  entry **cache = create_cache(associativity, idx_size, true);
  int VC_block;
  bool loadstore = rand() % 2; // 0=Load 1=Store
  for (int i = 0; i < associativity; i++)
  {
    randGeneration = true;
    while (randGeneration)
    {
      tag_random = rand() % 4096;
      random_address = tag_random;
      random_address << idx_size;
      random_address = random_address | idx_random;
      while (random_address == address_A)
      {
        tag_random = rand() % 4096;
        random_address = tag_random;
        random_address << idx_size;
        random_address = random_address | idx_random;
      }
      randGeneration = false;
      for (int j = 0; j < associativity; j++)
      {
        if (random_address == cache[idx_random][j].tag & cache[idx_random][j].valid == true)
        {
          randGeneration = true;
        }
      }
    }

    cache[idx_random][i].tag = tag_random;
    cache[idx_random][i].dirty = 0;
    cache[idx_random][i].valid = 1;
    cache[idx_random][i].rp_value = i;
    VC_block = tag_random;
    VC_block << idx_size;
    VC_block = random_address | idx_random;
  }

  //**********************5. Read or Write (choose the operation randomly) AddressA. Check 1,2,3 and 4.***************

  operation_result *VCop_result = new operation_result;
  operation_result *L1op_result = new operation_result;
  int *miss = new int;
  int *hits = new int;
  int *VC_hits = new int;
  int *dirty = new int;
  int *index = new int;
  int *tag_r = new int;

  status = lru_replacement_policy(idx_A,
                                  tag_A,
                                  associativity,
                                  loadstore,
                                  cache[idx_A],
                                  L1op_result,
                                  debug);

  int *VCtag = new int;
  VCaddress_tag_get(L1op_result->evicted_address, idx_A, idx_size, VCtag);
  status = lru_replacement_policy(*zero,
                                  *VCtag,
                                  *sixteen,
                                  loadstore,
                                  VC[0],
                                  VCop_result,
                                  debug);

  /**********************Checks******************/

  // *****************1. Check operation result is a HIT*****************
  if (loadstore)
  {
    EXPECT_EQ(L1op_result->miss_hit, MISS_STORE); //L1 miss store
  }
  else
  {
    EXPECT_EQ(L1op_result->miss_hit, MISS_LOAD); //L1 miss load
  }

  EXPECT_EQ(VCop_result->miss_hit, HIT_LOAD | HIT_STORE); //VC hit

  //**********2. Check LRU data in L1 line is swapped with AddressA data in the victim cache.********
  EXPECT_EQ(VC[0][0].tag, VC_block); //Checks if block entered VC

  // *************3. Check replacement policy values in L1 were updated properly.*************
  for (int i = 0; i < associativity; i++)
  {
    if (cache[idx_random][i].rp_value == 0)
    {
      EXPECT_EQ(cache[idx_random][i].tag, tag_A);
    }
    else
    {
      EXPECT_EQ((int)cache[idx_random][i].rp_value, i + 1);
    }
  }
  // *****4. Check dirty bit value of AddressA was changed accordingly with the operation performed*******
  if (loadstore)
  {
    EXPECT_EQ(cache[idx_random][associativity - 1].dirty, true); //Valid check
  }
  else
  {
    EXPECT_EQ(cache[idx_random][associativity - 1].dirty, dirty_random); //dirty check
  }
}




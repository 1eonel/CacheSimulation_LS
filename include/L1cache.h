/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h> 

/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
  LRU,
NRU,
 RRIP,
 RANDOM 
};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};

/*
 * STRUCTS
 */

/* Cache tag array fields */
struct entry {
 bool valid ;
 bool dirty;
 int tag ;
 uint8_t rp_value ;
};

/* Cache replacement policy results */
struct operation_result {
 enum miss_hit_status miss_hit;
 bool dirty_eviction;
 int  evicted_address;
};

/* 
 *  Functions
 * /
/*************************************************************/
/*
 * Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   int *tag_size,
                   int *idx_size,
                   int *offset_size);
/*************************************************************/
/* 
 * Get tag and index from address
 * 
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         int tag_size,
                         int idx_size,
                         int offset_size,
                         int *idx,
                         int *tag);

/*************************************************************/
/* 
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(hp) policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int srrip_replacement_policy (int idx,
                              int tag,
                              int associativity,
                              bool loadstore,
                              entry* cache_blocks,
                              operation_result* operation_result,
                              bool debug=false);
/*************************************************************/
/* 
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy (int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);

/*************************************************************/
/*
 * Creates a 2D Array to represent the Cache
 * Ways -> Columns
 * Sets -> Rows
 * 
 * LRU counter are initialized the following way
 * Way0 = 0 ; Way1 = 1 ; Way2 = 2; ... ; WayN = N
 * 
 * RRPV values are initialized all in Assoc-1.
 * 
 * [in] associativity: number of ways of the entry
 * [in] idx_size: number of bits of the index field
 * [in] rp: replacemen pol.
 * 
 * [return] cache
 */
entry **create_cache (int associativity,
                     int idx_size,
                     bool rp);

/*************************************************************/
/*Helper Functions*/
/*
 * Checks if int is power of 2
 * [in] v: value to check
 * [return] isPow: boolean
 */
bool pow_of_2(int v);

/*************************************************************/
/*Recieves all final results and counts to present to the user.*/
void  print_Sim(  int cachesize_kb,
                  int associativity,
                  int blocksize_bytes,
                  int CPU_time,
                  int AMAT,
                  double miss_rate,
                  double read_miss_rate,
                  int dirty_evictions,
                  int misses_Load,
                  int misses_Store,
                  int misses,
                  int hits_Load,
                  int hits_Store,
                  int hits,
                  int accesses);

/*************************************************************/


void get_Args(int argc,
              char *argv[],
              int *cachesize_kb,
              int *blocksize_bytes,
              int *associativity,
              bool *rp);






#endif

/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

/*************************************************************/
int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{  

   //Add return ERROR when needed.
   bool cachesize_check = pow_of_2(cachesize_kb);
   bool associativity_check = pow_of_2(associativity);
   bool blocksize_check = pow_of_2(blocksize_bytes);

   if (cachesize_check == false || associativity_check == false || blocksize_check == false){
         return ERROR;}   
   if(cachesize_kb < 0 || associativity <= 0 || blocksize_bytes <= 0){
      return ERROR;}

   /*Offset*/
   *offset_size = log2(blocksize_bytes);

   /*Index for different Associativities*/
   if (associativity > 0){
      *idx_size = log2((cachesize_kb * KB) / (blocksize_bytes * associativity));
   }
   else{
      *idx_size = log2((cachesize_kb * KB) / blocksize_bytes);
   }

   /*Tag*/
   *tag_size = ADDRSIZE - *idx_size - *offset_size;  
   
  return OK;
}
/*************************************************************/
void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{  
   /*Tag*/
   *tag = address >> (ADDRSIZE - tag_size);
   
   /*Index*/
   int maskIdx = pow(2, idx_size) - 1;
   *idx = address >> offset_size;
   *idx = *idx & maskIdx;
   
}
/*************************************************************/
int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{  
   /*Check for Errors*/
   if (idx<0 || tag<0){
      return ERROR;  }
   bool associativity_check = pow_of_2(associativity);
   if (associativity_check==false){
      return ERROR;
   }
    
    /*Flag for detencting a Hit. Helps with function flow.*/
   bool hit = false;
   bool RRPVmax = false;

   /***HIT***/
   /*Search for Tag in the all the sets*/
   
   for (int a = 0; a < associativity; a++){
      if (cache_blocks[a].valid && tag == cache_blocks[a].tag){
         /*Found matching Tag with valid up.*/     
         //cout<<"HIT"<<endl;
         hit = true;

         /*****Write Result****/
         /*Hit Load or Store?*/
         /*[in] loadstore: type of operation true if load false if store*/
         if (loadstore == true){ //Store
         result->miss_hit = HIT_STORE;
            cache_blocks[a].dirty = true;                               
         }else {                 //Load
            result->miss_hit = HIT_LOAD;
            cache_blocks[a].dirty = false;  
         }

         /*Dirty Eviction*/
         result->dirty_eviction = false;

         /*No Evicted Address when Hit*/
         result->evicted_address = 0;

         /*******Update Replacement Policy Value******/
         /* Hit => RRPV = 0 */
         cache_blocks[a].rp_value = 0;         

         /*Loop ends with hit*/
         break;
      }
   }

   /***MISS***/
   if (hit == false){
      while(RRPVmax==false){
         for (int a = 0; a < associativity; a++){

            /*Search for RRPV = Assoc-1*/
            if (cache_blocks[a].rp_value == associativity - 1){
               RRPVmax = true;
               /*****Write Result****/
               /*Dirty Eviction?*/
               if (cache_blocks[a].dirty == true){ //block is dirty
                  result->dirty_eviction = true;
               }else{                              //block is not dirty
                  result->dirty_eviction = false;
               }

               /*Miss Load or Store?*/
               if (loadstore == true){ //Store
               result->miss_hit = MISS_STORE;
                  cache_blocks[a].dirty = true;                               
               }else {                 //Load
                  result->miss_hit = MISS_LOAD;
                  cache_blocks[a].dirty = false;  
               }
               
               /*Evicted Address*/
               result->evicted_address = cache_blocks[a].tag;

               /*******Update Replacement Policy Values******/
               if (associativity <=2 ){
                  cache_blocks[a].rp_value = 0;
               }else{
                  cache_blocks[a].rp_value = 2;
               }       

               /****Execute block replacement*****/
               cache_blocks[a].valid = true;
               cache_blocks[a].tag = tag;
               
               /*Loop ends with miss*/
               break;
            }
         }
         if (RRPVmax == false){
            int n = associativity;
            while(n--){
               cache_blocks[n].rp_value++;
            }

         }
      }
   }
   return OK;
}

/*************************************************************/
int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{  
   /*Check for Errors*/
   if (idx<0 || tag<0){
      return ERROR;  }
   bool associativity_check = pow_of_2(associativity);
   if (associativity_check==false){
      return ERROR;
   }
   
    /**********Most Recently Used = Assoc - 1******/
    /**********Least Recently Used = 0 ************/

   
    /*Flag for detencting a Hit. Helps with function flow.*/
   bool hit = false;


   /***HIT***/
   /*Search for Tag in the all the sets*/
   
   for (int a = 0; a < associativity; a++){
      if (cache_blocks[a].valid && tag == cache_blocks[a].tag){
         /*Found matching Tag with valid up.*/     
         //cout<<"HIT"<<endl;
         hit = true;

         /*****Write Result****/
         /*Hit Load or Store?*/
         /*[in] loadstore: type of operation true if load false if store*/
         if (loadstore == true){ //Store
         result->miss_hit = HIT_STORE;
            cache_blocks[a].dirty = true;                               
         }else {                 //Load
            result->miss_hit = HIT_LOAD;
            /*cache_blocks[a].dirty = false;*/  
         }

         /*Dirty Eviction*/
         result->dirty_eviction = false;

         /*No Evicted Address when Hit*/
         result->evicted_address = 0;

         /*******Update Replacement Policy Values******/
         /*3 Cases:
            A-> rp_value[ref block] == MRU      {No rp_value decrement}
            B-> rp_value[ref block] == 0        {--All}
            C-> 0 < rp_value[ref block] < MAX   {-- only > rp_value[referenced block]} */

         //for (int i = 0; i < associativity; i++){

            if (cache_blocks[a].rp_value == 0)                           //B
               {for (int i = 0; i < associativity; i++){
                     if (cache_blocks[i].rp_value != 0){
                           cache_blocks[i].rp_value--;}
               }

            }else if(cache_blocks[a].rp_value < associativity -1){
               
               for (int i = 0; i < associativity; i++){                                                 //C
                  if (cache_blocks[i].rp_value > cache_blocks[a].rp_value){     //C
                                    
                     cache_blocks[i].rp_value--;
                  }
               }
            }
            cache_blocks[a].rp_value = associativity - 1; // Update MRU

         /*Loop ends with hit*/
         break;
      }
   }

   /***MISS***/
   if (hit == false){
      //cout<<"MISS"<<endl;

      for (int a = 0; a < associativity; a++){
         /*Search for LRU Way*/

         if (cache_blocks[a].rp_value == 0){ 
            /*****Write Result****/
            /*Dirty Eviction?*/
            if (cache_blocks[a].dirty == true){ //block is dirty
               result->dirty_eviction = true;
            }else{                              //block is not dirty
               result->dirty_eviction = false;
            }

            /*Miss Load or Store?*/
            if (loadstore == true){ //Store
            result->miss_hit = MISS_STORE;
               cache_blocks[a].dirty = true;                               
            }else {                 //Load
               result->miss_hit = MISS_LOAD;
               cache_blocks[a].dirty = false;  
            }
            
            /*Evicted Address*/
            result->evicted_address = cache_blocks[a].tag;

            /*******Update Replacement Policy Values******/
            /*1 Case:
               A-> rp_value[ref block] == 0  {--All}*/

            for (int i = 0; i < associativity; i++){
               if (cache_blocks[i].rp_value != 0){                                         
                  cache_blocks[i].rp_value--;
               }            
            }
            cache_blocks[a].rp_value = associativity - 1; // Update MRU

            /*Execute block replacement*/
            cache_blocks[a].valid = true;
            cache_blocks[a].tag = tag;
            
            /*Loop ends with miss*/
            break;

         }
      }
   }
   return OK;
}
/*************************************************************/

entry **create_cache (int associativity,
                     int idx_size,
                     bool rp)
{
   int ways = associativity;
   int sets = pow(2, idx_size);

   /*Cache Matrix creation*/
   entry **cache = new entry*[sets];
   for(int w = 0; w < sets; w++){
      cache[w] = new entry[ways];
   }

   /*Cache initialization*/
   if(rp ==true){
      /*LRU counters are initialized the following way*/
      /* Way0 = 0 ; Way1 = 1 ; Way2 = 2; ... ; WayN = N*/
      for (int i = 0; i < sets; i++){  

         for (int j = 0; j < ways; j++){
            
            cache[i][j].valid = false;
            cache[i][j].dirty = false;
            cache[i][j].tag = 0;
            cache[i][j].rp_value = j;

            
         }      
      }
   }else{
      for (int i = 0; i < sets; i++){  

         for (int j = 0; j < ways; j++){
            
            cache[i][j].valid = false;
            cache[i][j].dirty = false;
            cache[i][j].tag = 0;
            cache[i][j].rp_value = associativity-1;

            
         }      
      }

   }
   return cache;
}


/*Helper Functions*/

bool pow_of_2(int v){
   
   bool isPow = (v & (v - 1)) == 0;

   //f = v && !(v & (v - 1)); 0 = false
   return isPow;
}



void  print_Sim(  int cachesize_kb,
                  int associativity,
                  int blocksize_bytes,
                  int CPU_time,
                  int AMAT,
                  double miss_rate,
                  double read_miss_rate,
                  int dirty_evictions,
                  int misses_Load,
                  int misses_Store,
                  int misses,
                  int hits_Load,
                  int hits_Store,
                  int hits,
                  int accesses){
  cout << endl;
  cout << "************************************" << endl;
  cout << "  C A C H E   P A R A M E T E R S " << endl;
  cout << "************************************" << endl;
  cout << "Cache Size (KB): " << ".........." << cachesize_kb << endl;
  cout << "Cache Associativity: " << "......" << associativity << endl;
  cout << "Cache Block Size (bytes):" << ".." << blocksize_bytes << endl;
  //cout << "Replacement Policy:" << "........" << blocksize_bytes << endl;

    /* Print Statistics */

  cout << "************************************" << endl;
  cout << "S I M U L A T I O N   R E S U L T S" << endl;
  cout << "************************************" << endl;
  cout << "CPU time (cycles):" << "........." << CPU_time << endl;
  cout << "AMAT(cycles):" << ".............." << AMAT << endl;
  cout << "Overall: miss rate" << "........." << miss_rate << endl;
  cout << "Read miss rate:" << "............" << read_miss_rate << endl;
  cout << "Dirty evictions:" << "..........." << dirty_evictions << endl;
  cout << "Load misses:" << "..............." << misses_Load << endl;
  cout << "Store misses:" << ".............." << misses_Store << endl;
  cout << "Total misses:" << ".............." << misses << endl;
  cout << "Load hits:" << "................." << hits_Load << endl;
  cout << "Store hits:" << "................" << hits_Store << endl;
  cout << "Total hits:" << "................" << hits << endl;
  cout << "Total memory accesses:" << "....." << accesses << endl;
  cout << "************************************" << endl;
 

                  }






void get_Args(int argc,
              char *argv[],
              int *cachesize_kb,
              int *blocksize_bytes,
              int *associativity,
              bool *rp){
    string argvi;
    string argvi_plus1;               

    for (int i = 1; i < argc; i++){
    argvi = argv[i];
    argvi_plus1 = argv[i+1];

    if (argvi== "-t"){ *cachesize_kb = atoi(argv[i+1]); i++;
    }
    else
    if (argvi== "-l"){ *blocksize_bytes = atoi(argv[i+1]);i++;
    }
    else
    if (argvi== "-a"){ *associativity = atoi(argv[i+1]);i++;
    }
    else 
    if (argvi == "-rp"){ 
      if(argvi_plus1 == "lru"){
        *rp = true;
        i++;
          cout << *rp << endl;

      }else
      if (argvi_plus1=="srrip"){
        *rp = false;
        i++;
          cout << *rp << endl;
      }      
    } 
  }
}



#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <inttypes.h>
#include <debug_utilities.h>
#include <ctime>

using namespace std;

#define HIT_TIME 1
#define MISS_PENALTY 20


int main(int argc, char * argv []) {
  unsigned ti, tf;
  ti = clock();
  printf("Do something :), don't forget to keep track of execution time");
  /*Initializations*********************************************/
  int *cachesize_kb = new int;
  int *associativity = new int;
  int *blocksize_bytes = new int;
  int status;
  int IC;
  int IC_count = 0;
  
  int *tag_size = new int;
  int *idx_size = new int;
  int *offset_size = new int; 

  int *idx = new int;
  int *tag = new int;

  int dirty_evictions = 0;
  int misses_Load = 0;
  int misses_Store = 0;
  int hits_Load = 0;
  int hits_Store = 0;
  int hits = 0;
  int misses = 0;
  
  int accesses;
  int AMAT;
  int CPU_time;

  double miss_rate;
  double hit_rate;
  double read_miss_rate;

  bool *rp = new bool;
  bool loadstore;
  bool debug = false;

  long address;

  char num;
  char tracedata[8];
  struct operation_result op_result = {};

  /****************************************************************************************************/
  /* Parse argruments */
  get_Args(argc,
          argv,
          cachesize_kb,
          blocksize_bytes,
          associativity,
          rp); 
  /*****************************************************************/
  /*Cache creation*/
  status = field_size_get(*cachesize_kb,
                          *associativity,
                          *blocksize_bytes,
                          tag_size,
                          idx_size,
                          offset_size );
  if(status == ERROR){ cout << "field_size_get = ERROR" << endl; exit(0);}

  entry **cache = create_cache(*associativity, *idx_size, *rp);

  /* Get trace's lines and start your simulation */
  string hash = "#";
  int c = 0;

  while(hash == "#"){   c++;
        /*Read Trace*/
        /*Check for # */
        hash = "not hash lol";
        cin >> hash;
        /*if(c==20000){
         hash = "im out";        }*/

        /*Read LoadStore*/
        cin >> tracedata;
        loadstore = atoi(tracedata);

        /*Read Address*/
        cin >> hex >> address;

        /*Read IC*/
        cin >> tracedata;
        IC = atoi(tracedata);
        IC_count = IC_count + IC;

        /*Process Trace Info*/
        /*Get Tag & Index*/
        address_tag_idx_get(address,
                        *tag_size,
                        *idx_size,
                        *offset_size,
                        idx,
                        tag);

        /*Apply Replacement Algorithm*/
        if(*rp == true){
          status = lru_replacement_policy(
            *idx,
            *tag,
            *associativity,
            loadstore,
            cache[*idx], 
            &op_result,            
            debug);
          if(status == ERROR){ cout << "lru_replacement_policy = ERROR" << endl; exit(0);}

        }else{
          status = srrip_replacement_policy(
            *idx,
            *tag,
            *associativity,
            loadstore,
            cache[*idx],
            &op_result,            
            debug);
            if(status == ERROR){ cout << "srrip_replacement_policy = ERROR" << endl; exit(0);}
        }

      /*Post Result Operations*/
      /*Dirty Evictions*/
      if(op_result.dirty_eviction == true){
        dirty_evictions++;
      }

      /*Updates Hit-Miss counters*/
      /*Hacer Función!!!!*/
      switch (op_result.miss_hit){
        case MISS_LOAD:
          misses_Load++;
          break;
        case MISS_STORE:
          misses_Store++;
          break;
        case HIT_LOAD:
          hits_Load++;
          break;
        case HIT_STORE:
          hits_Store++;
          break;
        }
    }

  /*Final Hit-Miss count*/
  hits = hits_Load + hits_Store;
  misses = misses_Load + misses_Store;
  accesses = hits + misses;

  /*Hit/Miss Rate*/
  miss_rate = double(misses) / double(accesses);
  hit_rate = double(hits) / double(accesses);
  read_miss_rate = double(misses_Load) / double(misses);

  /*CPU Time*/
  CPU_time = IC_count + (MISS_PENALTY*misses_Load) + accesses;

  /*AMAT*/
  AMAT = HIT_TIME + miss_rate*MISS_PENALTY;

  /*********************************************************************/
  /* Print cache configuration */

  print_Sim(  *cachesize_kb,
              *associativity,
              *blocksize_bytes,
              CPU_time,
              AMAT,
              miss_rate,
              read_miss_rate,
              dirty_evictions,
              misses_Load,
              misses_Store,
              misses,
              hits_Load,
              hits_Store,
              hits,
              accesses);

tf = clock();
double execution_time = (double(tf-ti)/CLOCKS_PER_SEC);
cout << "Execution time:" << "............" << execution_time << endl;
return 0;
}



























 /*printf("%#010x\n", maskTag);
 
   cout << "_____________________________" << '\n';*/

   /*********************test  a create matrix
    * 
    * 
    *   int x = 10;
  int y = 10;
  entry **cache = create_cache(x, y);
  cout << "------------print"<< endl;
  for (int i = 0; i < x; i++){
    for (int j = 0; j < y; j++){
      printf("%" PRIu8 , cache[i][j].rp_value);
      cout << ",  ";
    }
    cout << endl;
    }

    ********************************************///**/

    /*




  cout << "------------print---idx:"<< *idx_size<<endl;
  for (int i = 0; i < pow(2,*idx_size); i++){
    for (int j = 0; j < associativity; j++){
      printf("%" PRIu8 , cache[i][j].rp_value);
      cout << ",  ";
    }
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    }

    exit(0);*/


  //long address = 0xDEADCAFE;


   /*address_tag_idx_get(address,
                         tag_size,
                         idx_size,
                         offset_size,
                         idxp,
                         tagp
   );*/

